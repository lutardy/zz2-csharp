﻿using System;

namespace TPNoté
{
    [Serializable]
    public class Contact
    {
        private string nom;
        private string prenom;
        private string courriel;
        private string societe;
        private string lien;
        private DateTime dateCreation;
        private DateTime dateModif;

        public Contact() // for serialization
        {
            nom = "";
            prenom = "";
            courriel = "";
            societe = "";
            lien = "";
        }
        public Contact(string nom, string prenom, string courriel, string societe, string lien)
        {
            this.nom = nom;
            this.prenom = prenom;
            this.courriel = courriel;
            this.societe = societe;
            this.lien = lien;
            dateCreation = DateTime.Now;
        }

        public string Nom
        {
            get => nom;
            set { nom = value; dateModif = DateTime.Now; }
        }

        public string Prenom
        {
            get => prenom;
            set { prenom = value; dateModif = DateTime.Now; }
        }

        public string Courriel
        {
            get => courriel;
            set { courriel = value; dateModif = DateTime.Now; }
        }

        public string Societe
        {
            get => societe;
            set { societe = value; dateModif = DateTime.Now; }
        }

        public string Lien
        {
            get => lien;
            set { lien = value; dateModif = DateTime.Now; }
        }

        public override string ToString()
        {
            return "[C] nom : " + nom + ", prenom = " + prenom + ", courriel = " + courriel + ", societe = " + societe + ", lien = " + lien;
        }
    }
}
