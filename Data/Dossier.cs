﻿using System;
using System.Collections.Generic;

namespace TPNoté
{
    [Serializable]
    public class Dossier
    {
        private string nom;
        public DateTime DateCreation { get; set; } 
        public DateTime DateModif { get; set; }
        public List<Contact> ListeContacts { get; set; } = new();
        public List<Dossier> ListeDossiersFils { get; set; } = new();

        public Dossier() // for serialization
        {
            Nom = "";
        }
        public Dossier(string nom)
        {
            this.nom = nom;
            DateCreation = DateTime.Now;
        }

        public string Nom
        {
            get => nom;
            set { nom = value; DateModif = DateTime.Now; }
        }

        public void AjouterDossierFils(Dossier d) // création du dossier dans le programme principal afin de l'ajouter à liste de tous les dossiers
        {
            DateModif = DateTime.Now;
            ListeDossiersFils.Add(d);
        }

        public void CreerContact(string nom, string prenom, string courriel, string societe, string lien)
        {
            Contact c = new Contact(nom, prenom, courriel, societe, lien);
            DateModif = DateTime.Now;
            ListeContacts.Add(c);
        }

        public void Afficher(int etage, string nomCourant) // procédure récursive affichant le nom du dossier et ses contacts et appelant cette fonction pour tous ces fils
        {
            String spaces = "";
            if (nom.Equals(nomCourant))
                Console.ForegroundColor = ConsoleColor.Red; // écrit le dossier courant en rouge, le reste en blanc
            for (int i = 0; i < etage; ++i)
                spaces += ' ';
            Console.WriteLine(spaces + "[D] " + Nom + " (Création " + DateCreation + ")");
            Console.ForegroundColor = ConsoleColor.White;
            foreach (Contact c in ListeContacts)
                Console.WriteLine(spaces + "| " + c.ToString());        
            foreach (Dossier d in ListeDossiersFils)            
                d.Afficher(etage+1, nomCourant);            
        }
    }
}
