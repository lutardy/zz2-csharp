﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Xml.Serialization;
using TPNoté;

namespace Sérialisation
{
    public class Serializer
    {
        private static readonly string Chemin = "C:\\Users\\" + Environment.UserName + "\\Documents\\ContactManager.db"; // nom du fichier

        public static void Serialize(List<Dossier> listeDossiers) // sérialisation avec BinaryFormatter à partir d'une liste de dossiers
        {
            Console.WriteLine("Enregistrement du fichier '" + Chemin + "'...");
            Hashtable addresses = new();
            FileStream fichier = new(Chemin, FileMode.Create);
            foreach (Dossier d in listeDossiers)
                addresses.Add(d.Nom, d);

            BinaryFormatter formatter = new();
            try
            {
                formatter.Serialize(fichier, addresses);
            }
            catch (SerializationException e)
            {
                Console.WriteLine("Failed to serialize. Reason: " + e.Message);
            }
            finally
            {
                fichier.Close();
            }

        }

        public static List<Dossier> Deserialize() // désérialisation avec BinaryFormatter pour recréer la liste de dossiers et gestion de fichier non existant
        {
            Console.WriteLine("Chargement du fichier '" + Chemin + "'...");
            List<Dossier> listeDossiers = new();
            FileStream fichier;
            Hashtable addresses = null;
            try
            {
                fichier = new(Chemin, FileMode.Open);
                try
                {
                    BinaryFormatter formatter = new BinaryFormatter();
                    addresses = (Hashtable)formatter.Deserialize(fichier);
                }
                catch (SerializationException e)
                {
                    Console.WriteLine("Failed to deserialize. Reason: " + e.Message);
                }
                finally
                {
                    fichier.Close();
                }
                foreach (DictionaryEntry de in addresses)
                    listeDossiers.Add((Dossier)de.Value);

                Console.WriteLine("Fichier '" + Chemin + "' chargé");
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Fichier " + Chemin + " non existant");
            }

            return listeDossiers;
        }

        public static void XmlSerialize(List<Dossier> listeDossiers, byte[] key) // sérialisation avec XmlSerializer à partir d'une liste de dossiers
        {
            Console.WriteLine("Enregistrement du fichier '" + Chemin + "'...");
            XmlSerializer xmlserializer = new(typeof(List<Dossier>));
            FileStream fileStream = new(Chemin, FileMode.Create);
            try
            {
                Console.WriteLine("Chiffrement du fichier '" + Chemin + "'...");
                using Aes aes = Aes.Create();
                aes.Key = key;
                byte[] iv = aes.IV;
                fileStream.Write(iv, 0, iv.Length);

                using CryptoStream cryptoStream = new(fileStream,aes.CreateEncryptor(), CryptoStreamMode.Write);
                using StreamWriter encryptWriter = new(cryptoStream);
                xmlserializer.Serialize(encryptWriter, listeDossiers);
                Console.WriteLine("Fichier '" + Chemin + "' chiffré");
                Console.WriteLine("Fichier '" + Chemin + "' enregistré");
            }
            catch (Exception e)
            {
                Console.WriteLine($"The encryption failed. {e}");
                xmlserializer.Serialize(fileStream, listeDossiers);
            }
        }

        public static List<Dossier> XmlDeserialize(byte[] key) // désérialisation avec BinaryFormatter pour recréer la liste de dossiers et gestion de fichier non existant
        {
            Console.WriteLine("Chargement du fichier '" + Chemin + "'...");
            List<Dossier> listeDossiers = new();
            XmlSerializer xmlserializer = new(typeof(List<Dossier>));
            try
            {
                FileStream fileStream = new(Chemin, FileMode.Open);
                try
                {
                    Console.WriteLine("Déchiffrement du fichier '" + Chemin + "'...");
                    using Aes aes = Aes.Create();
                    byte[] iv = new byte[aes.IV.Length];
                    int numBytesToRead = aes.IV.Length;
                    int numBytesRead = 0;
                    int n;
                    while (numBytesToRead > 0)
                    {
                        n = fileStream.Read(iv, numBytesRead, numBytesToRead);
                        if (n == 0) break;

                        numBytesRead += n;
                        numBytesToRead -= n;
                    }
                    using CryptoStream cryptoStream = new(fileStream, aes.CreateDecryptor(key, iv), CryptoStreamMode.Read);
                    using StreamReader decryptReader = new(cryptoStream);
                    listeDossiers = (List<Dossier>)xmlserializer.Deserialize(decryptReader);

                    Console.WriteLine("Fichier '" + Chemin + "' déchiffré");
                    Console.WriteLine("Fichier '" + Chemin + "' chargé");
                }
                catch (Exception ex)
                {
                    Console.WriteLine($"The decryption failed. {ex}");
                }
            }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Fichier " + Chemin + " non existant");
            }
            return listeDossiers;
        }
    }
}
