﻿using Sérialisation;
using System;
using System.Collections.Generic;
using System.Security.Cryptography;
using System.Security.Principal;
using System.Text;

namespace TPNoté
{
    class Program
    {
        static void Main()
        {
            String[] commandes = {""}; // tableau de chaînes de caractères contenant la commande rentrée par l'utilisateur
            String nom, prenom, courriel, societe, lien;
            Dossier root = new("root"); // dossier racine créé au lancement de l'application
            Dossier current = root; // au lancement de l'application, le dossier courant est le dossier racine
            List<Dossier> listeDossiers = new(); // liste de tous les dossiers créés
            listeDossiers.Add(current); // ajout du dossier racine à la liste des dossiers créés
            bool found;
            byte[] key = new byte[16];
            while (commandes[0] != "sortir")
            {
                Console.ForegroundColor = ConsoleColor.Green; // commande rentrée par l'utilisateur écrite en vert, réponse en blanc
                commandes = Console.ReadLine().Split(' '); // premier case du tableau = nom de commande, reste = arguments
                Console.ForegroundColor = ConsoleColor.White;
                switch (commandes[0])
                {
                    case "sortir":
                        Console.WriteLine("Au revoir");
                        break;
                    case "afficher":
                        root.Afficher(0, current.Nom); // On affiche l'entièreté de la hiérarchie de dossiers à partir du dossier racine, le dossier courant est en rouge
                        break;
                    case "charger":
                        if (commandes.Length > 1 && ASCIIEncoding.ASCII.GetBytes(commandes[1]).Length == 16)
                            key = ASCIIEncoding.ASCII.GetBytes(commandes[1]);
                        else
                            key = ASCIIEncoding.ASCII.GetBytes(WindowsIdentity.GetCurrent().User.ToString()[..16]);
                        listeDossiers = Serializer.XmlDeserialize(key); // récupération de la liste des dossiers créés à partir d'un fichier
                        foreach (Dossier d in listeDossiers){ // le dossier racine est mis à jour et devient le dossier courant
                            if (d.Nom == "root")
                            {
                                current = d;
                                root = d;
                            }
                        }
                        break;
                    case "enregistrer":
                        if (commandes.Length > 1 && ASCIIEncoding.ASCII.GetBytes(commandes[1]).Length == 16)
                            key = ASCIIEncoding.ASCII.GetBytes(commandes[1]);
                        else
                            key = ASCIIEncoding.ASCII.GetBytes(WindowsIdentity.GetCurrent().User.ToString()[..16]);
                        Serializer.XmlSerialize(listeDossiers, key); // enregistrement de la liste des dossiers créés dans un fichier
                        break;
                    case "ajouterDossier": // ajout d'un dossier si son nom est rentré par l'utilisateur. Il est ajouté aux fils du dossier courant et à la liste des dossiers créés
                        if(commandes.Length > 1)
                        {
                            nom = commandes[1];
                            Dossier newDossier = new(nom);
                            Console.WriteLine("Dossier '" + nom + "' ajouté sous " + current.Nom);
                            current.AjouterDossierFils(newDossier);
                            listeDossiers.Add(newDossier);
                            current = newDossier;
                        }
                        else
                            Console.WriteLine("Nom du dossier manquant");
                        break;
                    case "ajouterContact": // ajout d'un contact si ses informations sont toutes rentrées par l'utilisateur. Il est ajouté aux contacts du dossier courant
                        if (commandes.Length > 5)
                        {
                            nom = commandes[1];
                            prenom = commandes[2];
                            courriel = commandes[3];
                            societe = commandes[4];
                            lien = commandes[5];
                            Console.WriteLine("Contact '" + prenom + "' ajouté sous " + current.Nom);
                            current.CreerContact(nom, prenom, courriel, societe, lien);
                        }
                        else
                            Console.WriteLine("Argument manquant");
                        break;
                    case "nouveauCourant": // repositionnement du dossier courant si son nom est rentré par l'utilisateur et qu'il existe bel et bien
                        found = false;
                        if (commandes.Length > 1)
                        {
                            foreach (Dossier d in listeDossiers)
                            {
                                if (commandes[1].Equals(d.Nom))
                                {
                                    found = true;
                                    current = d;
                                }
                            }
                            if (!found)
                                Console.WriteLine("dossier non existant");
                        }
                        else
                            Console.WriteLine("Nom du dossier manquant");
                        break;
                    default: // si le nom de la commande rentrée est inconnue, affichage de la liste des commandes possibles
                        Console.WriteLine("Instruction inconnue");
                        Console.WriteLine("Commandes possibles :");
                        Console.WriteLine("sortir");
                        Console.WriteLine("afficher");
                        Console.WriteLine("charger 'clé de chiffrement'");
                        Console.WriteLine("enregistrer 'clé de chiffrement'");
                        Console.WriteLine("ajouterDossier 'nom du dossier'");
                        Console.WriteLine("ajouterContact 'nom du contact' 'prenom' 'courriel' 'societe' 'lien'");
                        Console.WriteLine("nouveauCourant 'nom du dossier'");
                        break;
                }
            }
        }
    }
}
